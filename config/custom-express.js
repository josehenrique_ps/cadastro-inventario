var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
const path = require('path');

module.exports = () => {
    var app = express();

    app.set('views', path.join(__dirname, '../views'));
    app.use( express.static( 'public' ) );
    app.set('view engine', 'ejs');

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(expressValidator());

    consign()
        .include('controllers')
        .into(app);

    return app;
}