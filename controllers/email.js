var nodeMailer = require('nodemailer');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const uri = "mongodb://:@localhost:27017/inventario-samsung";

module.exports = (app) => {

    var db;

    MongoClient.connect(uri, { useNewUrlParser: true }, (err, client) => {

        if (err) return console.log(err);
        db = client.db('inventario-samsung');

    });

    app.post('/email/cadastrar', (req, res) => {
        var dados = req.body;

        console.log(dados);

        db.collection('emails').save(dados, (err, result) => {
            if (err) return console.log(err);

            res.send('e-mail cadastrado');
        });
    });

    app.post('/email/enviar', (req, res) => {
        console.log('chegou no email');
        var transporter = nodeMailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'josehenrique.ps@gmail.com',
                pass: 'Jhps@85dmrv'
            }
        });

        var mailOptions = {
            from: '"José Henrique" <josehenrique.ps@gmail.com>',
            to: 'josehenrique.ps@gmail.com',
            subject: 'Resumo',
            text: 'texto do email',
            html: '<b>Teste negrito</b>'
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }

            console.log('Message %s sent: %s', info.messageId, info.response);
        });
    });

}