var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('../swagger.json');

module.exports = (app) => {

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
//app.use('/api/v1', router);

};