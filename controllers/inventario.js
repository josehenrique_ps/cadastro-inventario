var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const uri = "mongodb://:@localhost:27017/inventario-samsung";
var qrcode = require('qrcode');
const fs = require('fs');
var qrimage = require('qr-image');
var request = require('request');
var nodeMailer = require('nodemailer');
const multer = require('multer');
const upload = multer({ dest: 'public/' });

module.exports = (app) => {

    var db;

    MongoClient.connect(uri, { useNewUrlParser: true }, (err, client) => {

        if (err) return console.log(err);
        db = client.db('inventario-samsung');

    });

    app.get('/view', (req, res) => {
        res.render('lista_equipamento');
    });

    app.get('/cadastroEquipamento', (req, res) => {
        res.render('equipamento', { data: [] });
    });

    app.post('/save', upload.single('image'), function (req, res, next) {
        // req.file é o aquivo `image`
        // req.body terá os campos textos, se tiver algum
        console.log(req.file);
        console.log(req.body);

        if (req.file) {
            var file = 'public/' + new Date().getTime() + '.png';
            fs.rename(req.file.path, file, function (err) {
                if (err) {
                    console.log(err);
                    res.send(500);
                } else {
                    salvarEquipamento(req, res, req.body, db, file);
                }
            });
        } else {
            salvarEquipamento(req, res, req.body, db, 'nomeImagem');
        }
    });

    app.route('/equipamento')
        .get((req, res) => {

            db.collection('inventario').find().toArray((err, results) => {
                if (err) return console.log(err);

                res.render('lista_equipamento', { data: results });

            });

        })
        .post((req, res) => {
            var dados = req.body;
            var id = dados._id;

            if (id) {
                dados._id = new ObjectID(dados._id);
            }

            console.log(dados);

            console.log(req.body, req.files);

            upload.single('image'), (req, res) => {

                console.log(req.body, req.file);

                res.send('ok')

            }


            db.collection('inventario').save(dados, (err, result) => {

                if (err) return console.log(err);

                console.log('Salvando no banco de dados');
        
                var code = qrimage.image(JSON.stringify(result.ops), { type: 'png', ec_level: 'H', size: 5, margin: 0 });
                res.setHeader('Content-type', 'image/png');
                code.pipe(res);

                var filename = 'public/qrcode/' + new Date().getTime() + '.png';
                code.pipe(require('fs').createWriteStream(filename));

                enviarEmail(code, filename);

            });
        });

    app.get('/equipamento/editar/:id', (req, res) => {
        var id = req.params.id;

        db.collection('inventario').find(ObjectID(id)).toArray((err, results) => {
            if (err) return console.log(err);

            res.render('equipamento', { data: results });

        });
    });

    app.get('/equipamento/deletar/:id', (req, res) => {
        var id = req.params.id;
        db.collection('inventario').deleteOne({ _id: ObjectID(id) }, (err, result) => {
            if (err) return res.send(500, err);
            console.log('Deletado do Banco de Dados');
            res.redirect('/equipamento');
        });
    });
};

function enviarEmail(qrCode, localizacao, db) {
    console.log('chegou no email');
    var transporter = nodeMailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'josehenrique.ps@gmail.com',
            pass: 'Jhps@85dmrv'
        }
    });

    db.collection('emails').find().toArray((err, results) => {
        if (err) return console.log(err);

        var destinatarios;
        var separador = '';
        for (emailCadastrado of results) {
            destinatarios = separador + emailCadastrado.email;
            separador = ',';
        }

        var mailOptions = {
            from: '"José Henrique" <josehenrique.ps@gmail.com>',
            to: destinatarios,
            subject: 'Resumo',
            text: 'texto do email',
            html: 'Identificação Equipamento: <img src="cid:unique@kreata.ee"/>',
            attachments: [{
                filename: 'image.png',
                path: localizacao,
                cid: 'unique@kreata.ee' //same cid value as in the html img src
            }]
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }

            console.log('Message %s sent: %s', info.messageId, info.response);
        });

    });
};

function salvarEquipamento(req, res, dados, db, nomeImagem) {
    var id = dados._id;

    if (id) {
        dados._id = new ObjectID(dados._id);
    }

    console.log(dados);

    var dadosImagem = dados;
    dadosImagem.imagem = nomeImagem;
    console.log(dadosImagem);

    db.collection('inventario').save(dadosImagem, (err, result) => {

        if (err) return console.log(err);

        console.log('Salvando no banco de dados');
        console.log(result.ops);

        if (result.ops) {

            var code = qrimage.image(JSON.stringify(result.ops), { type: 'png', ec_level: 'H', size: 5, margin: 0 });
            res.setHeader('Content-type', 'image/png');
            code.pipe(res);

            var filename = 'public/qrcode/' + new Date().getTime() + '.png';
            code.pipe(require('fs').createWriteStream(filename));

            enviarEmail(code, filename, db);
        } else {
            res.send('Salvo com sucesso');
        }
    });
};